import React from 'react';
import request from 'request';
import logo from './logo.svg';
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={ActivateGetRequest}>
          GET request
        </button>

        <button onClick={ActivatePostRequest}>
          POST request
        </button>

        <button onClick={ActivatePutRequest}>
          PUT request
        </button>
      </header>
    </div>
  );
}


function ActivateGetRequest()
{
  request('https://localhost:5001/api/values/', function (error, response, body) {
  console.error('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  console.log('body:', body); // Print the HTML for the Google homepage.
  });
}

function ActivatePostRequest()
{
  fetch('https://localhost:5001/api/values/', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    firstParam: 'yourValue',
    secondParam: 'yourOtherValue',
  }),
});
}

function ActivatePutRequest()
{
  fetch('https://localhost:5001/api/values/2', {
    method: 'PUT',
    body: JSON.stringify({
      id: 1,
      title: 'foo',
      body: 'bar',
      userId: 1
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  }).then(response => {
          return response.json()
  }).then(json => {
    console.log(json)
    this.setState({
      user:json
    });
  })
}


export default App;
